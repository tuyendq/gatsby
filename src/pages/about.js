import * as React from 'react';

const AboutPage = ()  => {
    return (
        <main>
            <title>About Me</title>
            <h1>About Me</h1>
            <p>Learn . Practice . Share</p>
        </main>
    );
}

export default AboutPage;